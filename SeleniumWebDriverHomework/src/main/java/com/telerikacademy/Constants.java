package com.telerikacademy;

public class Constants {

    public static final String CHROME_DRIVER_HOME = "C:\\Users\\bg1000u3\\Desktop\\Programs\\" +
            "WEB Drivers\\chromedriver.exe";

    public static final String BING_URL = "https://www.bing.com/";
    public static final String GOOGLE_URL = "https://www.google.com/";
    public static final String SEARCH_TEXT = "Telerik Academy Alpha";
    public static final String TITLE = "IT Career Start in 6 Months - Telerik Academy Alpha";
    public static final String GOOGLE_CONSENT_BUTTON_CSS_SELECTOR = "#W0wltc > div";
    public static final String GOOGLE_SEARCH_BUTTON_BY_CSS_SELECTOR = ".FPdoLc input[name=btnK]";
    public static final String GOOGLE_SEARCH_RESULT_BY_CSS_SELECTOR = ".yuRUbf a h3";

    public static final String BING_INPUT_SEARCH_BY_ID = "sb_form_q";
    public static final String BING_SEARCH_BUTTON_BY_ID = "search_icon";
    public static final String BING_SEARCH_RESULT_BY_CSS_SELECTOR = ".b_topTitle a";
}
