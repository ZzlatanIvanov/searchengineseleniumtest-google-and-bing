import base.BaseTestSetup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import static com.telerikacademy.Constants.*;

public class SearchEngineTests extends BaseTestSetup {

    @Test
    public void bingSearch() {

        driver.get(BING_URL);
        WebElement inputSearch = driver.findElement(By.id(BING_INPUT_SEARCH_BY_ID));
        Assertions.assertTrue(inputSearch.isDisplayed(), "Search input is not displayed");
        inputSearch.sendKeys(SEARCH_TEXT);

        WebElement searchIcon = driver.findElement(By.id(BING_SEARCH_BUTTON_BY_ID));
        searchIcon.click();
        
        String searchResult = driver.findElement(By.cssSelector(BING_SEARCH_RESULT_BY_CSS_SELECTOR))
                .getText();
        Assertions.assertEquals(TITLE, searchResult, "Search result don't exist");
    }

    @Test
    public void googleSearch() {

        driver.get(GOOGLE_URL);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(GOOGLE_CONSENT_BUTTON_CSS_SELECTOR)))
                .click();
        WebElement inputSearch = driver.findElement(By.name("q"));
        Assertions.assertTrue(inputSearch.isDisplayed(), "Search input is not displayed");
        inputSearch.sendKeys(SEARCH_TEXT);

        WebElement searchButton = driver.findElement(By.cssSelector(GOOGLE_SEARCH_BUTTON_BY_CSS_SELECTOR));
        searchButton.click();

        String searchResult = driver.findElement(By.cssSelector(GOOGLE_SEARCH_RESULT_BY_CSS_SELECTOR))
                .getText();
        Assertions.assertEquals(TITLE, searchResult, "Search result don't exist");
    }
}
