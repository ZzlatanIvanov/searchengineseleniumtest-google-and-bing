package base;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.telerikacademy.Constants.CHROME_DRIVER_HOME;

public class BaseTestSetup {

    protected static WebDriver driver;

    @BeforeEach
    public void initialSettings() {
        System.setProperty("webdriver.chrome.driver",
                CHROME_DRIVER_HOME);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterEach
    public void closeBrowser() {
        driver.quit();
    }

}
